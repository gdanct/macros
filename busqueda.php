<?php
//conexion bd
$puerto = "localhost";
$usuario = "root";
$pass = "";
$bd = "macros";
$conexion = mysqli_connect($puerto, $usuario, $pass, $bd) or die("Error en la conexion");

//convertir string to date
//$time1 = strtotime($fecha_recepcion);
//$time2 = strtotime($fecha_recep_arch);
//$date1 = date('Y-m-d', $time1);
//$date2 = date('Y-m-d', $time2);




//query
$consulta = mysqli_query($conexion, "SELECT * from registro") or die("Error traer datos");

    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Control | Busqueda</title>
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <!-- Bootstrap 3.3.4 -->
            <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- Font Awesome Icons -->
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <!-- Ionicons -->
            <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
            <!-- DATA TABLES -->
            <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
            <!-- Theme style -->
            <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
            <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
            <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        </head>
        <body class="skin-blue sidebar-mini">
            <div class="wrapper">

                <header class="main-header">
                    <!-- Logo -->
                    <a href="index2.html" class="logo">
                        <!-- mini logo for sidebar mini 50x50 pixels -->
                        <span class="logo-mini"><b>Co</b>INT</span>
                        <!-- logo for regular state and mobile devices -->
                        <span class="logo-lg"><b>CONTROL</b> Interno</span>
                    </a>
                    <!-- Header Navbar: style can be found in header.less -->
                    <nav class="navbar navbar-static-top" role="navigation">
                        <!-- Sidebar toggle button-->
                        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                            <span class="sr-only">Toggle navigation</span>
                        </a>
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">


                            </ul>
                        </div>
                    </nav>
                </header>
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="main-sidebar">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">

                        <!-- sidebar menu: : style can be found in sidebar.less -->
                        <ul class="sidebar-menu">
                            <li class="header">MENU</li>
                            <li class="active treeview">
                                <a href="#">
                                    <i class="fa fa-dashboard"></i> <span>Formularios</span> <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="registro.html"><i class="fa fa-circle-o"></i> Registro</a></li>
                                    <li class="active"><a href="busqueda.php"><i class="fa fa-circle-o"></i> Busqueda</a></li>
                                </ul>
                            </li>
                        </ul>
                    </section>
                    <!-- /.sidebar -->
                </aside>

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Main content -->
                    <section class="content">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Tabla de datos</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th id="num_expediente">N° Expediente</th>
                                            <th id="num_servicio">N° servicio</th>
<!--                                            <th id="fecha_recepcion">fecha recepcion</th>
                                            <th id="fecha_recep_arch">fecha arch recp</th>-->
                                            <th id="ruc">RUC</th>
                                            <th id="razon_social">Razon Social</th>
<!--                                            <th id="num_doc">tipo de copia</th>
                                            <th id="notas">Notas</th>-->
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php while ($extraido = mysqli_fetch_array($consulta)) { 
                                                $id=$extraido['id_expediente'];
                                                ?>
                                            
                                            <td><?php echo $id ?></td>
                                            <td><?php echo $extraido['num_expediente'] ?></td>
<!--                                            <td><?php echo $extraido['fecha_recepcion'] ?></td>
                                            <td><?php echo $extraido['fecha_archivo_recepcion'] ?></td>-->
                                            <td><?php echo $extraido['ruc'] ?></td>
                                            <td><?php echo $extraido['razon_social'] ?></td>
<!--                                            <td><?php echo $extraido['num_doc'] ?></td>
                                            <td><?php echo $extraido['nota'] ?></td>-->
                                            <td>
                                                <a href="modificar.php?id=<?php echo $id; ?>"><button class="btn btn-block btn-info btn-flat">Modificar</button></a>
                                            </td>
                                             </tr>
                                            <?php
                                            }
                                            mysqli_close($conexion);
                                            ?>
                                   
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <!-- SlimScroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/app.min.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": false,
                    "autoWidth": false,
                    "scrollY": true,
                    "scrollX": true
                });
            });
        </script>
    </body>
</html>

